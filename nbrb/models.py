from django.db import models


class ExchangeRates(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateField()
    rate = models.FloatField()
    scale = models.IntegerField()
    abbreviation = models.CharField(max_length=4)


class IngotRates(models.Model):
    ingot_name = models.CharField(max_length=15)
    date = models.DateField()
    value = models.FloatField()
