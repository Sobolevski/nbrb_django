from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import ExchangeRates
from .models import IngotRates
from .currency_info import currency_info
from .forms import DateForm
from .ingot_info import ingot_info


NUMBER_OF_METALS = 4
NUMBER_OF_CURRENCY = 26


def index(request):
    template = loader.get_template('nbrb/index.html')
    return HttpResponse(template.render())


def price_metals(request):
    list_daily_price_ingots = IngotRates.objects.order_by('-pk')[:NUMBER_OF_METALS]
    template = loader.get_template('nbrb/price_metals.html')
    context = {
        'list_daily_price_ingots': list_daily_price_ingots,
        'date': list_daily_price_ingots[0].date
    }
    return HttpResponse(template.render(context))


def exchange_rates(request):
    list_daily_exrates = ExchangeRates.objects.order_by('-pk')[:NUMBER_OF_CURRENCY]
    template = loader.get_template('nbrb/exrates.html')
    context = {
        'list_daily_exrates': list_daily_exrates,
        'date': list_daily_exrates[0].date
    }
    return HttpResponse(template.render(context))


def info_currency(request, abbreviation):
    template = loader.get_template('nbrb/info_valute.html')
    context = {
        'img': currency_info[abbreviation]['img'],
        'info': currency_info[abbreviation]['info']
    }
    return HttpResponse(template.render(context))


def info_metal(request, name):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            list_daily_price_ingots = IngotRates.objects.filter(ingot_name=name)
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']
            list_of_metal = []
            for ingot in list_daily_price_ingots:
                if ingot.date >= start_date and ingot.date <= end_date:
                    list_of_metal.append(ingot)

            context = {
                'list_of_metal': list_of_metal,
                'form': form,
                'info': ingot_info[name],
                'name': name
            }
            return render(request, 'nbrb/info_metal.html', context)

    else:
        form = DateForm()
        context = {
            'text': 'Введите промежуток дат, за который нужно вывести цены',
            'form': form,
            'info': ingot_info[name],
            'name': name
        }
        return render(request, 'nbrb/info_metal.html', context)

