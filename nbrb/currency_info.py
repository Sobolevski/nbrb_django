currency_info = {
    'RUB': {
        'info': 'Росси́йский ру́бль — денежная единица Российской Федерации. Используется также на территории ряда непризнанных и частично признанных государств: Республики Абхазия, Республики Южная Осетия, Луганской Народной Республики и Донецкой Народной Республики.',
        'img': 'russia'
   },
    'USD': {
        'info': 'До́ллар Соединённых Штатов Америки (англ. United States dollar, МФА: [jʊˌnaɪ.tɪd ˈsteɪts ˈdɑlɚ]) — денежная единица США, одна из основных резервных валют мира. 1 доллар = 100 центов. Символьное обозначение в англоязычных текстах: $. Буквенный код валюты: USD. Правом денежной эмиссии обладает Федеральная резервная система (англ. Federal Reserve System), выполняющая в США функции центрального банка.',
        'img': 'us'
   },
    'CHF': {
        'info': 'Швейцарский франк является валютой и законным платёжным средством Швейцарии и Лихтенштейна. Банкноты франков выпускает центральный банк Швейцарии (Национальный банк Швейцарии), в то время как монеты выпускаются федеральным монетным двором (Швейцарский монетный двор).',
        'img': 'ch'
   },
    'SEK': {
        'info': 'Кро́на (швед. krona) — валюта Швеции. 1 крона формально делится на 100 э́ре (швед. öre). Монеты, номинированные в эре, в настоящее время не выпускаются.',
        'img': 'se'
   },
    'CZK': {
        'info': 'Чешская крона (чеш. Koruna česká) — денежная единица Чехии. Состоит из 100 геллеров (чеш. haléř). В 1993 году, после распада Чехословакии чешская крона сменила на территории нового государства чехословацкую крону. Одновременно в Словакии появилась словацкая крона.',
        'img': 'cz'
   },
    'GBP': {
        'info': 'Фунт сте́рлингов, фунт, британский фунт (англ. Pound sterling, Pound, British pound) — денежная единица, являющаяся: национальной валютой Соединённого Королевства Великобритании и Северной Ирландии (Великобритании), включающего Англию, Шотландию, Уэльс и Северную Ирландию. Параллельной валютой коронных земель Гернси, Джерси и Острова Мэн. Законным средством платежа для британских заморских территорий: Фолклендские острова, Гибралтар, Острова Святой Елены, Вознесения и Тристан-да-Кунья.',
        'img': 'gb'
   },
    'TRY': {
        'info': 'Турецкая ли́ра (тур. Türk Lirası) — денежная единица Турции. Введена 29 октября 1923 года в ходе реформы, проведённой Ататюрком, заменив османскую лиру. До 2005 года лира = 100 курушей = 4000 пар.',
        'img': 'tr'
   },
    'KZT': {
        'info': 'Казахста́нский тенге́ (каз. Қазақстан теңгесі; м. р. нескл., произносится (teŋˈgʲe, qɑzɑqsˈtɑn teŋgʲeˈsʲi) — национальная валюта Казахстана. Введена в обращение 15 ноября 1993 года.',
        'img': 'kz'
   },
    'KGS': {
        'info': 'Сом (кирг. сом) — национальная валюта Киргизии. Один сом равен ста тыйынам.',
        'img': 'kg'
   },
    'SGD': {
        'info': 'Сингапурский доллар — национальная валюта Сингапура (символ — $ или S$). Состоит из 100 центов.',
        'img': 'sg'
   },
    'XDR': {
        'info': 'Специальные права заимствования (СПЗ) или СДР (англ. Special Drawing Rights, SDR, SDRs) — искусственное резервное и платёжное средство, эмитируемое Международным валютным фондом (МВФ). Имеет только безналичную форму в виде записей на банковских счетах, банкноты не выпускались.',
        'img': 'xd'
   },
    'NOK': {
        'info': 'Кро́на (норв. krone, norsk krone, мн. ч. kroner) — национальная валюта Норвегии. Состоит из 100 эре (ед. и мн. ч. — øre).',
        'img': 'no'
   },
    'NZD': {
        'info': 'Новозеландский доллар (код валюты NZD) — валюта Новой Зеландии, Ниуэ, Островов Кука, Токелау, и Питкэрна. Официальная аббревиатура валюты — NZ$. Часто называют просто «киви» (в честь национальной птицы Новой Зеландии). NZ$ 1 = 100 центов.',
        'img': 'nz'
   },
    'MDL': {
        'info': 'Молда́вский лей (молд. leu, мн.ч. lei) — денежная единица Молдавии. В настоящее время в обращении находятся банкноты номиналами 1, 5, 10, 20, 50, 100, 200, 500 и 1000 леев, а также монеты достоинством 1, 5, 10, 25 и 50 баней (молд. ban, мн.ч. bani).',
        'img': 'md'
   },
    'KWD': {
        'info': 'Куве́йтский дина́р (араб. دينار كويتي‎) — национальная валюта эмирата Кувейт, равная 1000 филсам. В обращении находятся банкноты номиналами 1/4, 1/2, 1, 5, 10 и 20 динаров, а также монеты номиналами 1, 5, 10, 20, 50 и 100 филсов. Кувейтский динар — устойчивая свободно конвертируемая валюта, а также самая дорогая валюта в мире.',
        'img': 'kw'
   },
    'CNY': {
        'info': 'Юа́нь (кит. трад. 圓, упр. 元, пиньинь: Yuán) — современная денежная единица Китайской Народной Республики, в которой измеряется стоимость жэньминьби (кит. упр. 人民币, пиньинь: Rénmínbì, буквально: «народные деньги», сокращённо RMB). Одна из основных резервных валют мира, входит в «корзину» специальных прав заимствования МВФ.',
        'img': 'cn'
   },
    'CAD': {
        'info': 'Кана́дский до́ллар (фр. dollar canadien; англ. Canadian dollar, код ISO 4217 — CAD, сокращенное обозначение — $ или C$) — денежная единица Канады. Введена в 1858 году. Один канадский доллар состоит из 100 центов. В 2007 году канадский доллар занимал 7-е место среди мировых валют, наиболее активно торгуемых на валютном рынке. Значительное влияние на колебания курса канадского доллара оказывают экономика США и доллар США, в меньшей степени японская иена, китайский юань, евро.',
        'img': 'ca'
   },
    'JPY': {
        'info': 'Ие́на (яп. 円 эн) — денежная единица Японии, одна из основных резервных валют мира. Делится на 100 сен (счётная денежная единица, в 1954 году изъята из обращения). Международный код: JPY. Символом иены является знак ¥. В виде серебряных и золотых монет стала чеканиться в 1869—1871 годах.',
        'img': 'jp'
   },
    'ISK': {
        'info': 'Исла́ндская кро́на (исл. Íslensk króna)— денежная единица Исландии. Введена в 1885 году.',
        'img': 'is'
   },
    'IRR': {
        'info': 'Риа́л (перс. ریال‎) — денежная единица Персии с 1798 по 1825 год и Ирана c 1932 года по настоящее время.',
        'img': 'ir'
   },
    'PLN': {
        'info': 'Зло́тый (польск. złoty — «золотой») — как минимум с XV века обиходное название золотого дуката, с 1526 года основная счётная денежная единица, с 1564 года серебряная монета Королевства Польского, а после и Речи Посполитой, с 1924 года и по настоящее время национальная валюта Польши.',
        'img': 'pl'
   },
    'EUR': {
        'info': 'Е́вро (знак валюты — €, банковский код: EUR) — официальная валюта 19 стран «еврозоны» (Австрии, Бельгии, Германии, Греции, Ирландии, Испании, Италии, Кипра, Латвии, Литвы, Люксембурга, Мальты, Нидерландов, Португалии, Словакии, Словении, Финляндии, Франции, Эстонии)',
        'img': 'eu'
   },
    'DKK': {
        'info': 'Да́тская кро́на (дат. danske kroner) — национальная валюта Дании, имеющая также хождение в Гренландии и на Фарерских островах. 1 крона делится на 100 эре (дат. øre).',
        'img': 'dk'
   },
    'UAH': {
        'info': 'Гри́вна, иногда гри́вня (укр. гривня, IPA: /ˈɦrɪvnʲɑ/) — национальная валюта Украины, названная в честь древнерусской гривны, древней денежной единицы, представлявшей собой слиток серебра.',
        'img': 'ua'
   },
    'BGN': {
        'info': 'Лев (болг. лев) — денежная единица Болгарии, содержит 100 стотинок. С 2002 года курс лева привязан к евро и составляет 1,95583 лева за 1 евро.',
        'img': 'bg'
   },
    'AUD': {
        'info': 'Австрали́йский до́ллар (знак: $; код: AUD) — валюта Австралийского Союза, включая Острова Рождества, Кокосовые острова и Острова Норфолк, а также независимых тихоокеанских государств Кирибати, Науру и Тувалу. Делится на 100 центов. Обыкновенно сокращается знаком доллара ($), но существуют и другие варианты обозначения: A$, $A, AU$ и $AU.',
        'img': 'au'
   },
}