from django.conf.urls import url


from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^exrates', views.exchange_rates, name='exrates'),
    url(r'^priceMetals', views.price_metals, name='priceMetals'),
    url(r'^(?P<abbreviation>[A-Z]{3})', views.info_currency, name='info_currency'),
    url(r'^(?P<name>.+)', views.info_metal, name='info_metal')
]